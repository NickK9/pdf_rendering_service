# PDF rendering service

This is a service that accepts PDF files containing one or more pages. These pages
render to normalized png files and fit into 1200x1600 pixels rectangle.

The service is accessible through a REST API and offloads all file processing to asynchronous tasks by dramatiq library and Redis, so that it is easy to scale.


## Installation

1. Go to the pdf_api directory:

```bash
cd pdf_api
```

2. Start services by docker:

```bash
docker-compose up --build
``` 

3. Run migrations:

```bash
docker-compose run web python manage.py migrate
```

4. **If you want to run test, run command:**
```bash
docker-compose run \
  -e --no-deps --rm web py.test;
```
## API Usage

To upload file [file will be attach as form-data with key "file"]: 
```bash
http://0.0.0.0:8000/app/documents/  
```
To take info about document:
```bash
http://0.0.0.0:8000/app/documents/<int:document_id>/
```
To take page picture: 
```bash
http://0.0.0.0:8000/app/documents/<int:document_id>/pages/<int:page_number>/
```

## Future additions
1. Data storage on S3. 
2. Advanced tests. 
3. Improved application security.
