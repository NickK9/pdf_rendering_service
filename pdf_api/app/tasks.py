from wand.image import Image 
import PyPDF2
import io
import logging
import dramatiq

from .models import PdfDocument, DocumentPages


logging.basicConfig(format="%(asctime)s - %(levelname)s: %(message)s", filename="logs.log", level=logging.DEBUG)

@dramatiq.actor
def pdf_page_to_png(pdf_doc, resolution = 300, width = 1200, height = 1600):
    """
    This function splits pdf into pages, then converts pages to images
    """

    logging.info("Converting starts")
    try:

        src_pdf = PyPDF2.PdfFileReader(pdf_doc, strict=False) 
        dst_pdf = PyPDF2.PdfFileWriter()
        for pagenum in range(0, src_pdf.getNumPages()):
            dst_pdf.addPage(src_pdf.getPage(pagenum))

        pdf_bytes = io.BytesIO()
        dst_pdf.write(pdf_bytes)
        pdf_bytes.seek(0)

        img = Image(file = pdf_bytes, resolution = resolution)
        img.convert("png")

        img.resize(width, height)
        created_filename = pdf_doc.replace(".pdf", "_page.png")
        img.save(filename = created_filename)

        parent = pdf_doc.replace("media/", "")
        parent = PdfDocument.objects.get(file_name=parent)
        parent.pages_amount =  src_pdf.getNumPages()

        for page in range(0, src_pdf.getNumPages()):
            pages = DocumentPages(parent=parent, page_name=pdf_doc.replace(".pdf", f"_page-{page}.png"), page_number=page+1)
            pages.save()

        parent.status = True
        parent.save()

        logging.info("Method did it`s work successfuly")

    except Exception as e:
        logging.error("Error", str(e))
