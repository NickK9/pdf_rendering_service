from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("documents/", views.upload_file, name="upload_file"),
    path("documents/<int:document_id>/", views.document_info, name="document_info"),
    path("documents/<int:document_id>/pages/<int:page_number>/", views.get_document_page, name="get_document_page"),
]