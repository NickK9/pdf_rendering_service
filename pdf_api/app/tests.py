from django.test import TestCase
from django.urls import reverse
from django.test import Client
from django.core.files.uploadedfile import SimpleUploadedFile
from .models import PdfDocument, DocumentPages
from django.test import override_settings

import datetime
from PIL import Image
import tempfile
import base64


class UploadFileViewTests(TestCase):
    """
    Test for views.upload_file method
    """

    def setUp(self):
        client = Client()

    def test_empty_body(self):
        """
        Empty body without file
        """
        response = self.client.post(reverse("upload_file"))
        assert response.json() == {"error": "invalid params"}
        assert response.status_code == 200

    def test_correct_file(self):
        """
        Body with correct file
        """
        file = SimpleUploadedFile("test_file.pdf", b"something")
        response = self.client.post(reverse("upload_file"), {"file": file})
        
        assert response.json() == {"id": 1}
        assert response.status_code == 200

    def test_incorrect_file(self):
        """
        Not pdf file
        """
        video = SimpleUploadedFile("file.mp4", b"file_content", content_type="video/mp4")
        response = self.client.post(reverse("upload_file"), {"file": video})

        assert response.json() == {"error": "invalid params"}
        assert response.status_code == 200


class DocumentInfoViewTests(TestCase):
    """
    Test for views.document_info
    """

    def setUp(self):
        client = Client()
        PdfDocument.objects.create(file_name="test.pdf", upload_date=datetime.datetime.now(), pages_amount=2, status=True)

    def test_wrong_document_id(self):
        """
        Wrong document id
        """
        response = self.client.get(reverse("document_info", args=[2]))
        assert response.json() == {"error": "invalid params"} 
        assert response.status_code == 200

    def test_right_document(self):
        """
        Right document id finished
        """
        response = self.client.get(reverse("document_info", args=[2]))
        assert response.json() == {"status": "done", "n_pages": 2}
        assert response.status_code == 200


class GetDocumentPageViewTests(TestCase):
    """
    Test for views.get_document_page
    """

    def setUp(self):
        client = Client()
        
        PdfDocument.objects.create(file_name="test2.pdf", upload_date=datetime.datetime.now(), pages_amount=2, status=True)
        doc = PdfDocument.objects.get(file_name="test2.pdf")

        DocumentPages.objects.create(parent=doc, page_name=doc.file_name.replace(".pdf", f"_page-{1}.png"), page_number=1)

        size = (200, 200)
        color = (255, 0, 0, 0)
        image = Image.new("RGBA", size, color)
        image.save(doc.file_name.replace(".pdf", f"_page-{1}.png"), "png")

    def test_wrong_doc_id(self):
        """
        Wrong document id
        """
        response = self.client.get(reverse("get_document_page", args=[5, 5]))
        assert response.json() == {"error": "invalid params"}
        assert response.status_code == 200

    @override_settings(MEDIA_ROOT=tempfile.gettempdir())
    def test_right_ids(self):
        """
        Right document id finished
        """
        doc = PdfDocument.objects.get(file_name="test2.pdf")
        pages = DocumentPages.objects.get(parent=doc)
        with open(pages.page_name, "rb") as image_file:
                image_data = base64.b64encode(image_file.read()).decode("utf-8")

        response = self.client.get(reverse("get_document_page", args=[4, 1]))
        assert response.json() == {"image": image_data}
        assert response.status_code == 200
