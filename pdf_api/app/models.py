from django.db import models

class PdfDocument(models.Model):
    file_name = models.CharField("filename in directory", max_length=255)
    upload_date = models.DateTimeField("date uploaded")
    pages_amount = models.IntegerField(null=True)
    status = models.BooleanField(default=False)

class DocumentPages(models.Model):
    parent = models.ForeignKey(PdfDocument, on_delete=models.CASCADE) # the document that owns the page
    page_name = models.CharField("filename in directory", max_length=255)
    page_number = models.IntegerField(default=0)
    