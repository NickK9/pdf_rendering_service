from django.http import HttpResponse, JsonResponse
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_exempt
from .models import PdfDocument, DocumentPages
from django.core.files.storage import FileSystemStorage
from .forms import UploadFileForm

from datetime import datetime
import base64
import logging
import dramatiq

from .tasks import pdf_page_to_png


logging.basicConfig(format="%(asctime)s - %(levelname)s: %(message)s", filename="logs.log", level=logging.DEBUG)

@csrf_exempt
def index(request):
    logging.info("Main page")
    return HttpResponse("Hello, This is great API for converting PDF files to PNG images. You can find documentation in README file")

@csrf_exempt
@require_http_methods(["POST"])
def upload_file(request):
    """
    uploads a file
    returns JSON { “id”: “<DOCUMENT_ID>? }
    """
    logging.info("Upload file starts")
    if request.FILES:
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():

            logging.info("Form was valid")

            file = request.FILES["file"]
            if file.name[-4:] == ".pdf":
                fs = FileSystemStorage()
                filename = fs.save(file.name, file)
                pdf_doc = PdfDocument(file_name=filename, upload_date=datetime.now())
                pdf_doc.save()

                pdf_page_to_png.send("media/" + pdf_doc.file_name)
                
                logging.info("Method did it`s work successfuly")

                return JsonResponse({"id": pdf_doc.id})

    logging.error("Request without files or with wrong files")
    return JsonResponse({"error": "invalid params"})

@csrf_exempt
@require_http_methods(["GET"])
def document_info(request, document_id):
    """
    returns JSON { “status”: “processing|done”, “n_pages”: NUMBER }
    """
    logging.info("Document info starts")
    try:
        document = PdfDocument.objects.get(id=document_id)
        logging.info("Method did it`s work successfuly")
        return JsonResponse({"status": "done" if document.status else "processing", "n_pages": document.pages_amount})
    except:
        logging.error("Request without correct information")
        return JsonResponse({"error": "invalid params"})

@csrf_exempt
@require_http_methods(["GET"])
def get_document_page(request, document_id, page_number):
    """
    return bytes image png
    """
    logging.info("Get document page starts")
    try:
        page = DocumentPages.objects.get(parent__id=document_id, page_number=page_number)
        try:
            with open(page.page_name, "rb") as image_file:
                image_data = base64.b64encode(image_file.read()).decode("utf-8")
            logging.info("Method did it`s work successfuly")
            return JsonResponse({"image": image_data})
        except Exception as e:
            logging.error(str(e))
            return JsonResponse({"error": str(e)})
    except:
        logging.error("Request without correct information")
        return JsonResponse({"error": "invalid params"})
